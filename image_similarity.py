# -*- coding: utf-8 -*-
from __future__ import division
import argparse
from scipy import misc
from skimage import img_as_float
from skimage.color import rgb2gray
from skimage.measure import structural_similarity
import os
import itertools
import sys
from multiprocessing import Pool
from flask import Flask, render_template, request, redirect, url_for
import Image
import webbrowser
import random
import threading


# image similarity
THRESHOLD = None
images = None
deleted = set()

parser = argparse.ArgumentParser(description='Findet doppelte Bilder in einem Verzeichnis')
parser.add_argument('paths', metavar='Pfad', type=str, nargs='+',
                    help='die zu durchsuchenden Pfade (Dateien oder Ordner)')
parser.add_argument('-g', metavar='Wert', dest='threshold', type=float, default=0.9,
                    help='die Genauigkeit (ein Wert zwischen 0.0 und 1.0)') 
parser.add_argument('-r', dest='recursive', action='store_true',
                    help='Unterordner miteinbeziehen')

def filenames(paths, recursive=False):
    files = set([os.path.abspath(file) for file in args.paths if os.path.isfile(file)])
    for dir in paths:
        if not os.path.isdir(dir):
            continue
        if recursive:
            for root, _, fs in os.walk(dir):
                files.update((os.path.abspath(os.path.join(root,f)) for f in fs))
        else:
            for f in os.listdir(dir):
                f = os.path.abspath(os.path.join(dir, f))
                if os.path.isfile(f):
                    files.add(f)
    return files

def read_image(filepath):
    try:
        im = misc.imread(filepath)
        im = misc.imresize(im, (256,256))
        return (filepath, rgb2gray(img_as_float(im)))
    except IOError:
        return None

def to_array(files):
    pool = Pool()
    it = pool.imap(read_image, files)
    arrays = dict(filter(lambda f: f is not None, it))

    pool.close()
    pool.join()
    
    return arrays

def score(t):
    im1 = t[0]
    im2 = t[1]
    sim = structural_similarity(im1[1], im2[1], dynamic_range=im1[1].max()-im1[1].min())
    if sim >= THRESHOLD:
        return (im1[0], im2[0], sim)
    else:
        return None

def duplicates(filearray, threshold):
    print u'Vergleiche die Bilddateien...'
    total = sum((1 for _ in itertools.combinations(filearray.items(), 2)))
    i = 0

    ds = set()
    pool = Pool()
    it = pool.imap(score, itertools.combinations(filearray.items(), 2))
    
    for res in it:
        sys.stdout.write(u"\rFortschritt: %d%%" % (i / total * 100))
        sys.stdout.flush()
        if res is not None:
            ds.add(res)
        i += 1

    pool.close()
    pool.join()

    sys.stdout.write('\rFertig! %d Duplikat(e) gefunden.\n' % len(ds))
    sys.stdout.flush()
    return filter(lambda res: res is not None, ds)


# web frontend
app = Flask(__name__)

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

def clear_temp_dir():
    for f in os.listdir('./static/temp'):
        if f.startswith('.'):
            continue
        path = os.path.join(os.path.abspath('./static/temp'), f)
        os.remove(path)

@app.route('/')
@app.route('/view/<int:id>')
def view(id=0):
    clear_temp_dir()
    ims = []
    s = None
    if images and len(images) > id:
        if any((i in deleted for i in (images[id][0], images[id][1]))):
            return view(id+1)
        ims = []
        im1 = Image.open(images[id][0])
        im2 = Image.open(images[id][1])
        for i, im in enumerate([im1, im2]):
            _, filename = os.path.split(images[id][i])
            path = os.path.join(os.path.abspath('./static/temp'), filename)
            im.save(path)
            ims.append((images[id][i], im1.size, filename))
        ims.sort(key=lambda x: x[1][0], reverse=True)
        s = images[id][2]
    return render_template('view.html', images=ims, score=s, id=id, total=len(images))

@app.route('/delete/<int:id>')
def delete(id):
    global deleted
    path = request.args.get('path')
    assert(path in images[id])
    assert(os.path.isfile(path))
    os.remove(path)
    deleted.add(path)
    return redirect(url_for('view', id=id+1))

@app.route('/shutdown/')
def shutdown():
    shutdown_server()
    clear_temp_dir()
    return 'Fahre den Server herunter.'


if __name__ == "__main__":
    args = parser.parse_args()
    THRESHOLD = args.threshold

    print 'Lade Bilder...'
    files = to_array(filenames(args.paths, args.recursive))
    images = duplicates(files, args.threshold)

    if images:
        port = 5000 + random.randint(0, 999)
        url = "http://127.0.0.1:{}".format(port)
        threading.Timer(1.25, lambda: webbrowser.open(url) ).start()
        app.run(port=port, debug=False)
